<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Messages;
use Illuminate\Support\Facades\Crypt;
use App\Events\MessageSent;
use Illuminate\Support\Facades\Response;
use App\Models\Chat;
use Illuminate\Support\Facades\Storage;

class MessagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeMessages(Request $request)
    {
        /*
         * Validates Request
         * finds current chat
         * extends chat object changes attribute by one
         * save
         */
        $this->validate(request(), [
            'message' => 'required',
            'chat_id' => 'required',
        ]);
        $chat = Chat::find($request->input('chat_id'));
        $chat->changes = $chat->changes + 1;
        $chat->save();
        /*
        *Encrypts chat message
        * and stores Message object with data
        */

        $encrypted = Crypt::encryptString($request->input('message'));
        $store = new Messages();
        $store->sender_id = \Auth::user()->id;
        $store->message = $encrypted;
        $store->chat_id = $request->input('chat_id');
        $store->save();
        return response()->json(['success' => 'success'], 200);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessages()
    {
        /*
         *get current chat key
         */
        $ch = new ChatController();
        $key = $ch->ChatKeyHelper();
        /*
         *get chat object with messages
         */
        $res = Chat::where('chat_key', '=', $key)->with('messages')->first();
        /*
         *transforms Chat collection to decrypt messages
         */
        $apo = $res->messages->transform(function ($item, $key) {
            $item['message'] = Crypt::decryptString($item->message);
            return $item;
        });
        /*
         *returns chat object with decrypted messages
         */
        return Response::json($res);
    }

}
