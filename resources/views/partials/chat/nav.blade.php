{{--  Simple sticky Navbar buttons opens modals  --}}
<nav class="navbar sticky-top navbar-light " style="background-color: #89DA59;">
    <a class="navbar-brand" href="#"></a>
    <div class="text-center">
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#notifications">
            Notifications <span id="num" class="badge badge-light"></span>
        </button>
    </div>
    <button class="btn btn-outline-success my-2 my-sm-0" data-toggle="modal" data-target="#search">Search</button>
    <!-- Button trigger modal -->
</nav>