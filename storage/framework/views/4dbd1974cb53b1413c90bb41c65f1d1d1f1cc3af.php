<!-- Modal -->
<style>
    button {
        appearance: button;
        -moz-appearance: button;
        -webkit-appearance: button;
        text-decoration: none;
        font: menu;
        color: ButtonText;
        display: inline-block;
        padding: 2px 8px;
        float: right;
    }
</style>
<div class="modal fade" id="notifications" tabindex="-1" role="dialog" aria-labelledby="notificationsTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="notificationsTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="getnotif">
                <div class="clearfix">
                    
                    <div v-for="notif in notification" class="card" style="width:100%;">
                        <div v-for="no in notif">
                            <div>
                                <div class="card-body" style="width: 100%">


                                    <img
                                            :src="no.profile.avatar"
                                            style="float: left; width: 80px;height: 80px;   object-fit: cover; border-radius: 50%;"
                                    >

                                    <h5 class="card-title" style="padding-left: 30%"
                                        :key="no.id"
                                        v-text="no.name"
                                    >
                                    </h5>
                                    <p class="card-text" :key="no.id" v-text="no.email"
                                       style="padding-left: 30%">

                                    </p>
                                    
                                    <form id="form" method="POST" action="/friends/delete_req"
                                          accept-charset="UTF-8">
                                        <p class="card-text" style="clear:right">

                                            <input name="delete" type="hidden" value="1"/>
                                            <input name="sender_id" type="hidden" :value=" id=  no.id">
                                            <input name="_token" type="hidden" value="<?php echo e(csrf_token()); ?>"/>

                                            <div class="form-group">
                                                <div class="control">
                                        <p style="float:left">Accept chat invite</p>
                                        <button class="btn btn-grey" style="">delete</button>
                                    </form>
                                </div>

                            </div>
                            
                            <form id="form" method="POST" action="/friends/accept_req"
                                  accept-charset="UTF-8">
                                <input name="approve" type="hidden" value="1"/>
                                <input name="sender_id" type="hidden" :value="no.id"/>
                                <input name="_token" type="hidden" value="<?php echo e(csrf_token()); ?>"/>

                                <div class="form-group">
                                    <div class="control">
                                        <button class="btn btn-primary">Accept</button>
                                    </div>

                                </div>
                            </form>


                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
    </div>
</div>
</div>
</div>
</div>