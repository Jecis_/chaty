<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    protected $fillable = ['sender_id', 'message', 'image', 'chat_id'];

    public function chat()
    {
        return $this->hasOne('App\Models\Chat');
    }

}
