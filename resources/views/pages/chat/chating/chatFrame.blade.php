@extends('layouts.chat_layout')
@section('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <link href="{{ asset('/assets/css/chat_nav.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/chat_search.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/chat_messages.css') }}" rel="stylesheet" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('/assets/emoji/lib/css/emoji.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/css/messageinput.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/fancybox/jquery.fancybox.min.css') }}"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/default-skin/default-skin.css"/>


    <style>
        #mod {
            width: 100%;
            height: auto;
            padding: 0;
            max-width: 100%;
        }

        #mod-con {
            height: 100%;
            min-height: 100%;
        }

        .img-responsive {
            object-fit: cover;

        }

        body {
            font-family: 'Bitter', Georgia, Times, sans-serif;
        }

        h1 {
            margin: 2em;
        }

        figure {
            height: auto;
            display: inline-block;
            width: 33.333%;
            float: left;
            padding: 0 10px 0 10px;
        }

        img {
            width: 100%;
            max-height: 100%;
        }

        video {
            height: auto;
            display: inline-block;
            width: 33.333%;
            float: left;
            padding: 0 10px 0 10px;
        }

        .spacer {
            height: 5em;
        }
    </style>
@stop
@section('content')

    <div id="wrapper" class="toggled">
        <!-- Sidebar -->
        <div id="sidebar-wrapper" style="max-width: 70%">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    @include('pages.chat.showConvos')
                </li>
            </ul>
        </div>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div id="datafetch">
                <div class="container-fluid" style="">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1>Chat</h1>
                            {{--
                                                        Vue loop trough data to access array info
                            --}}
                            <div v-for="(value,key) in data " v-if="key === 'messages'">

                                <div v-for="(key2,value2) in value">
                                    <div v-if="key2.sender_id === userID">
                                        <div class="outgoing_msg" style="width:100%">
                                            <div class="sent_msg">
                                                {{--
                                                                                                if sender ID is equal to user id show messages as outgoing
                                                --}}
                                                <p>@{{ key2.message }} </p>
                                                <span class="time_date">@{{ key2.created_at }}</span></div>
                                        </div>
                                    </div>
                                    {{--
                                                                        Else Show messages as incoming
                                    --}}
                                    <div v-else class="mesgs" style="width:100%!important; margin-top: 0px!important;">
                                        <div class="msg_history">
                                            <div class="incoming_msg">
                                                <div class="incoming_msg_img"><img style="width: 50px;height: 50px"
                                                                                   src="{{$sendUser->profile->avatar}}"
                                                                                   alt="sunil"></div>
                                                <div class="received_msg">
                                                    <div class="received_withd_msg">
                                                        <p>@{{ key2.message }}</p>
                                                        <span class="time_date">@{{ key2.created_at }}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <button type="button" class="btn btn-success" data-toggle="modal"
                        data-target="#images" style="display: inline-flex;margin-left: 80px">
                    Show Media
                </button>

                <div class="modal fade" id="images" tabindex="-1" role="dialog"
                     aria-labelledby="imagesLabel" aria-hidden="true"
                >
                    <div id="mod" class="modal-dialog" role="document">
                        <div id="mod-con" class="modal-content">
                            <div class="modal-header">

                                <h5 class="modal-title" style="margin-right: auto" id="imagesLabel">Media</h5>
                                <button style="margin-left: auto" type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">
                                    &times;
                                </button>
                            </div>
                            <div class="modal-body">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#Pictures"
                                           role="tab" aria-controls="Pictures" aria-selected="true">Pictures</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#Videos" role="tab"
                                           aria-controls="Videos" aria-selected="false">Videos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#Files" role="tab"
                                           aria-controls="Files" aria-selected="false">Files</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">

                                    <div class="tab-pane fade show active" id="Pictures" role="tabpanel"
                                         aria-labelledby="home-tab"
                                    >

                                        <div class="forcedgallery">
                                            <div id="gallery" class="gallery" itemscope
                                                 itemtype="http://schema.org/ImageGallery">
                                                {{--
                                                                                                Create gallery from only images in array fileData
                                                --}}
                                                <div v-for="(val,key3) in fileData ">
                                                    <div v-for="(key4,value2) in val.files">
                                                        <div v-if="
                                                            key4.file_type === 'jpg'
                                                            || key4.file_type=== 'jpeg'
                                                            || key4.file_type=== 'png'
                                                        ">
                                                            <!-- Use figure for a more semantic html -->
                                                            <figure itemprop="associatedMedia" itemscope
                                                                    itemtype="http://schema.org/ImageObject">
                                                                <a class="fancybox" :href="key4.file_location"
                                                                   :data-caption="key4.file_name"
                                                                   data-width="1920" data-height="1080"
                                                                   data-fancybox-group="gallery1" itemprop="contentUrl">
                                                                    <!-- Thumbnail -->
                                                                    <img class="img-responsive"
                                                                         :src="key4.file_location"
                                                                         itemprop="thumbnail"
                                                                         alt="Image description"
                                                                    >
                                                                </a>
                                                            </figure>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Videos" role="tabpanel"
                                         aria-labelledby="profile-tab">
                                        {{--
                                                                                Find videos in fileData array
                                        --}}
                                        <div v-for="(val,key3) in fileData ">
                                            <div v-for="(key4,value2) in val.files">
                                                <div v-if="key4.file_type === 'mp4'">
                                                    <video width="585.4" height="312.083" controls>
                                                        <source :src="key4.file_location" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Files" role="tabpanel"
                                         aria-labelledby="contact-tab">
                                        {{--
                                                    Find all other files in fileData array
                                        --}}
                                        <div v-for="(val,key3) in fileData ">
                                            <div v-for="(key4,value2) in val.files">
                                                <div v-if="
                                                key4.file_type !== 'mp4'
                                                            && key4.file_type !== 'jpg'
                                                            && key4.file_type!== 'jpeg'
                                                            && key4.file_type!== 'png'
                                                ">
                                                    <ul>
                                                        <li>
                                                            <a :href="key4.file_location">
                                                                @{{ key4.file_name }}
                                                            </a>
                                                        </li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                        {{--
                                                                                End loop
                                        --}}
                                    </div>
                                    {{--
                                                                        End Tab
                                    --}}
                                </div>
                            </div>
                            {{--
                                                        End Modal Body
                            --}}
                        </div>
                    </div>
                </div>
            </div>
            {{--
            End Vue Datafetch instance
            --}}
            <div id="submitting">
                <div class="type_msg">

                    <div class="message-box">
                        <div class="input_msg_write">
                            {{--
                                                        Form to send messages without reloading
                            --}}
                            <form id="form" method="POST" action=""
                                  @submit.prevent="onSubmit"
                                  accept-charset="UTF-8"
                                  @keydown="form.errors.clear($event.target.name)"
                            >


                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                <input type="text" name="message" class="message-input"
                                       placeholder="Type message..." data-emojiable="true" v-model="form.message"/>
                                <span class="alert alert-danger"
                                      v-if="form.errors.has('message') "
                                      v-text="form.errors.get('message')">
                                </span>
                                <input id="id_for_chat" name="chat_id" type="hidden"/>
                                <button type="submit" class=" btn btn-warning" style="display: inline-table">Send
                                </button>
                            </form>

                            @include('pages.chat.Modals.addfile')
                            {{--
                                                        Get add file Modal to send files
                            --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--
    Photoswipe Container Do not touch
    --}}
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="pswp__bg"></div>
        <div class="pswp__scroll-wrap">

            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <div class="pswp__ui pswp__ui--hidden">
                <div class="pswp__top-bar">
                    <div class="pswp__counter"></div>
                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                    <button class="pswp__button pswp__button--share" title="Share"></button>
                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>
                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>
                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>
                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>
            </div>
        </div>
    </div>
    {{--
    End of Photoswipe container
    --}}
@stop
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/photoswipe-ui-default.js"></script>
    <script src="{{asset('assets/emoji/lib/js/config.js')}}"></script>
    <script src="{{asset('assets/emoji/lib/js/util.js')}}"></script>
    <script src="{{asset('assets/emoji/lib/js/jquery.emojiarea.js')}}"></script>
    <script src="{{asset('assets/emoji/lib/js/emoji-picker.js')}}"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
        window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel;
        const Auth = 'Bearer '+ '<?php echo \Auth::user()->api_token ?>';
        axios.defaults.headers.common['Authorization'] = Auth;


        /*
        Form class handles file sending to server
        */
        class Form {
            constructor(data) {
                this.originalData = data;
                for (let field in data) {
                    this[field] = data[field]
                }
                this.errors = new Errors();

            }

            data() {
                let data = Object.assign({}, this);
                delete data.originalData;
                return data;
            }

            reset() {
                for (let field in originalData) {
                    this[field] = '';
                }
            }

            submit(requestType, url) {
                axios[requestType](url, this.data())
                    .then(this.onSuccess.bind(this))
                    .catch(this.onFail.bind(this));

                $('.emoji-wysiwyg-editor').empty();
                /*Clears emoji input */
            }

            onSuccess(response) {
                document.getElementById('form').reset();
                this.message = null;
                this.errors.clear();
                this.reset();
            }

            onFail(error) {

                this.errors.record(error.response.data);


            }
        }

        /*
        * Get variables From PHP to JS
        */
        let id = '{{$mess->id}}';
        id = parseInt(id);
        let user_id = '{{ Auth::user()->id}}';
        user_id = parseInt(user_id);
        /*
        * Vue to send messages
        */
        new Vue({
            el: '#submitting',
            data: {
                form: new Form({
                    message: null,
                    chat_id: id,
                }),
            },
            methods: {
                onSubmit() {
                    this.form.submit('post', '/api/v1/chat/msg');

                },
            }
        });
        /*
        *Vue to get data from server
        */
        new Vue({
            el: '#datafetch',
            data() {
                return {
                    data: [],
                    fileData: [],
                    userID: user_id,
                    changesFirst: '',
                    changing: '',

                }
            },
            methods: {
                /*
                * Scroll method will go to page bottom on load and when is loaded new message
                */
                scroll: function () {
                    $('html, body').animate({
                            scrollTop: $(document).height() - $(window).height()
                        },
                        400,
                        "swing"
                    );
                },
                /*
                * Gallery method handles Photoswipe JS To create gallery
                * Gallery updates when new images are received
                */
                gallery: function () {
                    'use strict';
                    /* global jQuery, PhotoSwipe, PhotoSwipeUI_Default, console */
                    (function ($) {
                        // Init empty gallery array
                        var container = [];
                        // Loop over gallery items and push it to the array
                        $('#gallery').find('figure').each(function () {
                            var $link = $(this).find('a'),
                                item = {
                                    src: $link.attr('href'),
                                    w: $link.data('width'),
                                    h: $link.data('height'),
                                    title: $link.data('caption')
                                };
                            container.push(item);
                            /*
                                                                console.log(item)
                            */
                        });
                        // Define click event on gallery item
                        $('.fancybox').click(function (event) {
                            // Prevent location change
                            event.preventDefault();
                            // Define object and gallery options
                            var $pswp = $('.pswp')[0],
                                options = {
                                    index: $(this).parent('figure').index(),
                                    bgOpacity: 0.85,
                                    showHideOpacity: true
                                };
                            // Initialize PhotoSwipe
                            var gallery = new PhotoSwipe($pswp, PhotoSwipeUI_Default, container, options);
                            gallery.init();
                        });
                    }(jQuery));
                },
                /*
                * On page Load get current chat message count
                */
                countervar: function () {
                    axios.get('{{route('listen')}}').then((response) => {
                        this.changesFirst = response.data;
                    });
                },
                /*
                 * Get New chat message count
                */
                GetChanges: function () {
                    axios.get('{{route('listen')}}').then((response) => {
                        this.changing = response.data;
                    });
                },
                /*
                 * Get Messages
                 */
                loadData: function () {
                    axios.get('/api/v1'+window.location.pathname + '/messages').then((response) => {
                        this.data = response.data;
                    });
                    axios.get('{{route('file.file')}}').then((response) => {
                        this.fileData = response;
                        /*//console.log(response.fileData);
                        $.each(response, function (value, key) {
                            $.each(key.files, function (one, two) {
                                console.log(two.file_location);
                            })
                        });*/

                    });
                }
            },
            /*
            *When component created
            * get counter
            * get changes
            * both needs to be same on load
            * lod messages payload
            * when loaded
            * create gallery
            * scroll to bottom
            */
            created() {
                this.countervar();
                this.GetChanges();
                this.loadData();
                setTimeout(function () {
                    this.gallery();
                    this.scroll();
                }.bind(this), 1000);
                /*
                *Every seven seconds check if there is new payload
                * if there is run load data request to server
                * rerun gallery instance to show in gallery new media
                * send person back to bottom to see new messages
                */
                setInterval(function () {
                    this.GetChanges();
                    if (this.changesFirst !== this.changing) {
                        this.changesFirst = this.changesFirst + 1;
                        this.loadData();
                        this.gallery();
                        this.scroll();
                    }
                }.bind(this), 4000);
            },
        });

        /*
        *Hamburger nav click function opens left side of page and shows chats and user info
        */
        $(".hamburger").click(function (e) {
            $("#wrapper").toggleClass("toggled");
        });
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
            emojiable_selector: '[data-emojiable=true]',
            assetsPath: '/assets/emoji/lib/img',
            popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
    </script>
@stop