<?php

namespace App\Http\Controllers;

use App\Models\FileHandler;
use App\Models\Chat;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class FileHandlerController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request)
    {
        /*
         *Find chat by id
         */
        $chat = Chat::find($request->input('chat_id'));
        /*
         *add +1 to chat object changes
         */
        $chat->changes = $chat->changes + 1;
        $chat->save();
        /*
         *Check if request has file
         */
        if ($request->hasFile('file')) {
            /*
             *foreach request file do following
             */
            foreach ($request->file as $file) {
                $firstId = $chat->first_user_id;
                $secondId = $chat->second_user_id;
                $chat_id = $request->input('chat_id');
                /*
                 *Check if chat already have files directory
                 */
                $existingdir = Chat::where('id', '=', $request->input('chat_id'))->with('files')->first();
                $newdir = $existingdir->files->first();
                /*
                 *If chat already have directory get its name and assign it to variable $masterDir
                 */
                if (isset($newdir)) {
                    $masterDir = $newdir->file_dir;
                }
                $fUsr = User::where('id', '=', $firstId)->first();
                $sUsr = User::where('id', '=', $secondId)->first();
                $firstPart = $fUsr->name;
                $secondPart = $sUsr->name;
                /*
                 *From $master string I hash directory name
                 */
                $master = $firstPart . $secondPart . $request->input('chat_id');
                $dirName = md5($master);
                /*
                 * I get file name and add some other parameters for randomness to create unique file hashed name
                 */
                $name = $file->getClientOriginalName();
                $newName = md5($name . $firstPart . $chat->changes);
                $extension = $file->getClientOriginalExtension();
                $rightextension = strtolower($extension);
                /*
                 *If chat dont have dir create one and store files
                 */
                if (!isset($masterDir)) {
                    Storage::makeDirectory('public/upload/' . $dirName);
                    $file->storeAs('public/upload/' . $dirName, $newName . '.' . $rightextension);
                    /*
                     *Store files in DB too
                     */
                    $fileModel = new FileHandler();
                    $fileModel->file_name = $name;
                    $fileModel->hash_name = $newName;
                    $fileModel->sender_id = \Auth::user()->id;
                    $fileModel->file_location = '/storage/upload/' . $dirName . '/' . $newName . '.' . $rightextension;
                    $fileModel->file_type = $rightextension;
                    $fileModel->file_dir = $dirName;
                    $fileModel->chat_id = $chat_id;
                    $fileModel->save();
                } else {
                    /*
                     *if Chat directory exists just save files there
                     * and in DB
                     */
                    $file->storeAs('public/upload/' . $masterDir, $newName . '.' . $rightextension);
                    $fileModel = new FileHandler();
                    $fileModel->file_name = $name;
                    $fileModel->hash_name = $newName;
                    $fileModel->sender_id = \Auth::user()->id;
                    $fileModel->file_location = '/storage/upload/' . $masterDir . '/' . $newName . '.' . $rightextension;
                    $fileModel->file_type = $rightextension;
                    $fileModel->file_dir = $masterDir;
                    $fileModel->chat_id = $chat_id;
                    $fileModel->save();
                }
            }
        }
        return redirect()->back();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFiles()
    {
        /*
         *Get chat files and return to frontend
         */
        $ch = new ChatController();
        $key = $ch->ChatKeyHelper();
        $result = Chat::where('chat_key', '=', $key)->with('files')->first();
        return Response::json($result, 200);
    }
}
