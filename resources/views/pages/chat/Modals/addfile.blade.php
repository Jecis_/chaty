{{--  Add file button and modal Form sends files to named route  --}}
<form action="{{route('upload.upload')}}" enctype="multipart/form-data" method="post"
      class="form-group">
    {{--  Display errors if any  --}}
    <span>
        @if($errors->any())
            <h4>{{$errors->first()}}</h4>
        @endif
    </span>
    <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}"/>
    <button type="button" class="btn btn-info" data-toggle="modal"
            data-target="#upload" style="display: inline-flex;">
        Add Files
    </button>

    <div class="modal fade" id="upload" tabindex="-1" role="dialog"
         aria-labelledby="uploadLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="uploadLabel">Update File</h5>
                </div>
                <div class="modal-body">
                    <p>Choose Files</p>
                    <input type="file" name="file[]" multiple>
                    <input type="hidden" name="chat_id" value="{{$mess->id}}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                    </button>
                    <input type="submit" value="Save" class="btn btn-info">
                </div>
            </div>
        </div>
    </div>
</form>