<?php

namespace App\Http\Controllers;

use App\Models\Friends;
use App\Models\User;
use Illuminate\Http\Request;

use Auth;
use Illuminate\Support\Facades\Response;
use Validator;

class ListController extends Controller
{
    protected $Friends;

    public function setProperty($name, $val)
    {
        return $this->$name = $val;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getProperty($name)
    {
        return $this->$name;
    }

    /**
     * @return mixed
     */
    public function listConvo()
    {
        $Friends = [];
        /*
         *Find friends
         */
        $send = Friends::where('sender_id', '=', \Auth::user()->id)->where('aprooved', '=', 1)->get();
        $recieve = Friends::where('reciever_id', '=', \Auth::user()->id)->where('aprooved', '=', 1)->get();
        /*
         *check if any of queries returns true
         */
        if (isset($send) || isset($recieve)) {
            /*
             *loop trough friend requests and assign user profiles to Friends array
             */
            if ($send->count() !== 0) {
                foreach ($send as $s) {
                    $Friends[] = User::where('id', '=', $s->reciever_id)
                        ->with('profile')
                        ->with('sender')
                        ->with('reciever')
                        ->get();

                }
            }
            if ($recieve->count() !== 0)
                foreach ($recieve as $re) {
                    $Friends[] = User::where('id', '=', $re->sender_id)
                        ->with('profile')
                        ->with('sender')
                        ->with('reciever')
                        ->get();
                }


        }
        /*
         *return Friends array with Users
         */
        return $this->setProperty('Friends', $Friends);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    /*
     *Home view get friends with auth current user data and return them to frontend
     */
    public function index()
    {
        /*
         *gets info from nav controller this is because thees functions applies to chat_frame and home views
         */
        $chatC = new NavController();
        $AuthUserInfo = $chatC->UserInfoForNavAndList();
        $this->listConvo();
        $Friends = $this->getProperty('Friends');
        return view('pages.chat.list_convos', compact('Friends','AuthUserInfo'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {

        $auth_id = \Auth::user()->id;

        $key = $request->keywords;

        if ($key == '') {
            $posts = [];
        } else {
            /*
             *Search logic returns users which names are similar or same as query in frontend
             */
            $posts = User::where('name', 'like', $key . '%')
                ->where('id', '!=', $auth_id)
                ->with('profile')
                ->with('sender')
                ->with('reciever')
                ->get();
        }

        $results = ['posts' => $posts];

        return Response::json($results);
    }
}
