<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $img;

    /*
     *Function to hold random images
     */
    private function rndIMG()
    {
        $items = ['/images/icons/Bear.png',
            '/images/icons/Beaver.png',
            '/images/icons/Butterfly.png',
            '/images/icons/Cat.png',
            '/images/icons/Dog.png',
            '/images/icons/Dragonfly.png',
            '/images/icons/Gorilla.png',
            '/images/icons/Hummingbird.png',
            '/images/icons/Panda.png',
            '/images/icons/Unicorn.png',
            '/images/icons/Turtle.png',
            '/images/icons/Wolf.png'
        ];
        /**
         * @var  img
         * @return img
         */

        $this->img = $items[array_rand($items)];
        return $this->img;
    }

    protected $attributes = [
        'avatar_status' => '1'
    ];

    /*
     *Assign random image to profile avatar and cal parent constructor
     */
    public function __construct(array $attributes = array())
    {
        $this->setRawAttributes(array_merge($this->attributes, array(
            'avatar' => $this->rndIMG()
        )));
        parent::__construct($attributes);
    }


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id',
    ];

    /**
     * Fillable fields for a Profile.
     *
     * @var array
     */
    protected $fillable = [
        'theme_id',
        'location',
        'bio',
        'twitter_username',
        'github_username',
        'user_profile_bg',
        'avatar',
        'avatar_status',
        'gravatar'
    ];

    protected $casts = [
        'theme_id' => 'integer',
    ];

    /**
     * A profile belongs to a user.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Profile Theme Relationships.
     *
     * @var array
     */
    public function theme()
    {
        return $this->hasOne('App\Models\Theme');
    }
}
