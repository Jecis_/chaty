<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(){

        return view('pages.testing');

    }
    public function recieve(Request $request){
        $message = $request->input('name');
        dd($message);
        event(new MessageSent($message));
    }
}
