<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
class NavController extends Controller
{
    /**
     * @return mixed
     */
    public function parseUrl()
    {
        /*
         *Helper function to extract in which page User are
         */
        $results = trim(url()->current());
        $part = explode('/', $results);
        $key = $part[3];
        return $key;
    }

    /**
     * @return mixed
     */
    public function UserInfoForNavAndList()
    {
        /*
         *Get current user info
         */
        $AuthUserInfo = User::where('id', '=', \Auth::user()->id)->with('profile')->first();
        return $AuthUserInfo;
    }
}
