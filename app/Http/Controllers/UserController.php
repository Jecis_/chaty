<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\User;
use App\Models\Friends;
use ReCaptcha\Response;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
         *creates home page with possible chats
         */

        $user = Auth::user();
        $nav = new NavController();
        $AuthUserInfo = $nav->UserInfoForNavAndList();
        $key = $nav->parseUrl();
        $list = new ListController();
        $list->listConvo();
        $Friends = $list->getProperty('Friends');
        $user_token = User::find($user->id);
        $user_token->api_token = $user->createToken('MyApp')->accessToken;
        $user_token->save();
        if ($user->isAdmin()) {
            return view('pages.chat.list_convos', compact('Friends', 'AuthUserInfo', 'key'));
        }
        return view('pages.chat.list_convos', compact('Friends', 'AuthUserInfo', 'key'));
    }
    public function getUser(){
        return Auth::guard('api')->user();
    }

}
