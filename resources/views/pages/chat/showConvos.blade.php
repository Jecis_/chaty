{{--  Retrieves possible conversations from server  --}}
<div class="clearfix">
    <div class="card" style="width:100%;">
        <div>
            <div class="card-body" style="width: 100%; background-color: whitesmoke">
                {{--  Get Auth user profile picture  --}}
                <img src="{{$AuthUserInfo->profile->avatar}}" style="float: left; width: 80px;height: 80px;   object-fit: cover; border-radius: 50%;">
                <h5 class="card-title" style="padding-left: 30%">
                    {{--  Get username  --}}
                    {{$AuthUserInfo->name}}
                </h5>
                <div class="card-text" style="padding-left: 30%">
                    {{--  Get email  --}}
                    {{{$AuthUserInfo->email}}}
                    <hr>
                    <div class="btn-group" role="group" style="text-align: center">
                        <div class="vl"></div>
                        {{--  Link to profile  --}}
                        <a class="btn btn-warning btn-sm" href="../profile/{{\Auth::user()->name}}" role="button">Profils</a>
                        {{--  get key to know in which page you are in chat show Chats but in home show nothing  --}}
                        @if($key === 'home')

                        @else
                            <div class="vl"></div>
                            <a class="btn btn-danger btn-sm" href="/home" role="button">Chats</a>
                        @endif
                        <div class="vl"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
{{--  Check if you have friends  --}}
@if(count($Friends) >0)
    <div class="clearfix">
        {{--  Loop trough your riends and display them in cards  --}}
        @foreach($Friends as $friend)
            @foreach($friend as $display)
                <div class="card" style="width:100%;">
                    <div>
                        <div class="card-body" style="width: 100%">
                            <img
                                    src="{{$display->profile->avatar}}"
                                    style="float: left; width: 80px;height: 80px;   object-fit: cover; border-radius: 50%;"
                            >
                            <h5 class="card-title" style="padding-left: 30%">
                                {{$display->name}}
                            </h5>
                            <p class="card-text"
                               style="padding-left: 30%">
                                {{{$display->email}}}
                                {{--  This block checks for possible chat keys so you can start chat  --}}
                                @if(count($display->sender)>0)
                                    @foreach ($display->sender as $sen)
                                        @if($sen->reciever_id === \Auth::user()->id)
                                            <a class="btn btn-primary" role="button" href="/chat/{{$sen->chat_key}}">Open
                                                Chat</a>
                                        @endif
                                    @endforeach
                                @endif
                                @if(count($display->reciever)>0)
                                    @foreach ($display->reciever as $rec)
                                        @if($rec->sender_id === \Auth::user()->id)
                                            <a class="btn btn-primary" role="button" href="/chat/{{$rec->chat_key}}">Open
                                                Chat</a>
                                        @endif
                                    @endforeach
                                @endif
                            </p>
                            <p class="card-text" style="clear:right">
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        @endforeach
    </div>
@endif