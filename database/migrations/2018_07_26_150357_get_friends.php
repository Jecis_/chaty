<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GetFriends extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('friend_req',function (Blueprint $table){
            $table->increments('id');
            $table->integer('sender_id');
            $table->integer('reciever_id');
            $table->boolean('aprooved')->default(0)->nullable();
            $table->integer('friend');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
