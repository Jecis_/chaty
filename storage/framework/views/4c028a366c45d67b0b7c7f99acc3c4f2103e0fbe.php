<?php $__env->startSection('styles'); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Bootstrap core CSS -->
    <!-- Custom fonts for this template -->
    <link href="<?php echo e(asset('assets/vendor/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="<?php echo e(asset('assets/vendor/magnific-popup/magnific-popup.css')); ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo e(asset('assets/css/creative.min.css')); ?>" rel="stylesheet">

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <div id="page-top">

        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand js-scroll-trigger" href="#page-top">Chaty</a>
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                        data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link js-scroll-trigger" href="#services">About</a>
                        </li>
                        <?php if(Route::has('login')): ?>
                                <?php if(Auth::check()): ?>
                                    <li class="nav-item" style="padding-right: 15px">
                                        <a class="btn btn-primary" role="button"  href="<?php echo e(url('/home')); ?>">Home</a>
                                    </li>
                                <?php else: ?>
                                    <li class="nav-item" >
                                        <a class="btn btn-info" role="button" style="margin-right: 15px!important;"  href="<?php echo e(url('/register')); ?>">Register</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="btn btn-primary" role="button" href="<?php echo e(url('/login')); ?>">Login</a>
                                    </li>

                                <?php endif; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <header class="masthead text-center text-white d-flex" style="background: url('<?php echo e(asset('assets/img/header.jpg')); ?>')">
            <div class="container my-auto">
                <div class="row">
                    <div class="col-lg-10 mx-auto">
                        <h1 class="text-uppercase">
                            <strong>Chaty</strong>
                        </h1>
                        <hr>
                    </div>
                    <div class="col-lg-8 mx-auto">
                        <p class="text-faded mb-5">Chaty simple and secure  chat app! P.S. We dont sell your data</p>
                        <a class="btn btn-primary btn-xl js-scroll-trigger" href="<?php echo e(url('/register')); ?>">Register</a>
                    </div>
                </div>
            </div>
        </header>

        <section id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">About Chaty</h2>
                        <hr class="my-4">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box mt-5 mx-auto">
                            <i class="fa fa-4x fa-diamond text-primary mb-3 sr-icons"></i>
                            <h3 class="mb-3">Easy to use</h3>
                            <p class="text-muted mb-0">Register and start searching for friends</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box mt-5 mx-auto">
                            <i class="fa fa-4x fa-paper-plane text-primary mb-3 sr-icons"></i>
                            <h3 class="mb-3"> Secure</h3>
                            <p class="text-muted mb-0">You can use chaty securely because we encrypt your messages</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box mt-5 mx-auto">
                            <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
                            <h3 class="mb-3">Up to Date</h3>
                            <p class="text-muted mb-0">We update dependencies to keep things fresh.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box mt-5 mx-auto">
                            <i class="fa fa-4x fa-heart text-primary mb-3 sr-icons"></i>
                            <h3 class="mb-3">Made with Love</h3>
                            <p class="text-muted mb-0">Chaty is open sorce thriveing to be better</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
    <!-- Bootstrap core JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo e(asset('assets/vendor/jquery-easing/jquery.easing.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/vendor/scrollreveal/scrollreveal.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/vendor/magnific-popup/jquery.magnific-popup.min.js')); ?>"></script>

    <!-- Custom scripts for this template -->
    <script src="<?php echo e(asset('assets/js/creative.js')); ?>}"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.chat_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>