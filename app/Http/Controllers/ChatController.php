<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Chat;
use Illuminate\Support\Facades\Response;


class ChatController extends Controller
{
    /**
     * @param $chat_key
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @return . Friends,mess,sendUser,AuthUserInfo,key
     */
    public function index($chat_key)
    {
        /*
         *create new navController
         * get user info
         * get Key
         */
        $userCon = new NavController();
        $AuthUserInfo = $userCon->UserInfoForNavAndList();
        $key = $userCon->parseUrl();
        /*
         *Get Friends Collection
         */
        $list = new ListController();
        $list->listConvo();
        $friendy = '';
        $Friends = $list->getProperty('Friends');
        /*
         *Get Chat where chat_key is equal to url Chat_key
         */
        $mess = Chat::where('chat_key', '=', $chat_key)->firstOrFail();
        $user = Chat::where('chat_key', '=', $chat_key)->firstOrFail();
        /*
         *Check in chat which user is current user and store result
         */
        if ($user->first_user_id === \Auth::user()->id) {
            $friendy = $user->second_user_id;
        } elseif ($user->second_user_id === \Auth::user()->id) {
            $friendy = $user->first_user_id;
        }
        /*
         *get current user with profile
         */
        $sendUser = User::where('id', '=', $friendy)->with('profile')->first();
        //dd($sendUser);

        return view('pages.chat.chating.chatFrame', compact('Friends', 'mess', 'sendUser', 'AuthUserInfo', 'key'));
    }

    /**
     * @return $key
     */
    public function ChatKeyHelper()
    {
        /*
         *helper function to extract chat_key from url
         */
        $results = trim(url()->previous());
        $part = explode('/', $results);
        $key = $part[4];
        return $key;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function listen()
    {
        /*
         *Returns changes in chat object changes
         */
        $key = $this->ChatKeyHelper();
        $res = Chat::where('chat_key', '=', $key)->firstOrFail();
        $changes = $res->changes;
        return Response::json($changes);

    }
}
