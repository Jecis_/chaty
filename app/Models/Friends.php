<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Awobaz\Compoships\Compoships;

class Friends extends Model
{
    use Compoships;

    protected $table = 'friend_req';

    protected $fillable = ['sender_id','reciever_id','aprooved','friend'];

    public function users(){
      return  $this->belongsTo('App\Models\User');
    }
}
