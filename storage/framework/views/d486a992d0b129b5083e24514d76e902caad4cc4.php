
<div class="clearfix">
    <div class="card" style="width:100%;">
        <div>
            <div class="card-body" style="width: 100%; background-color: whitesmoke">
                
                <img src="<?php echo e($AuthUserInfo->profile->avatar); ?>" style="float: left; width: 80px;height: 80px;   object-fit: cover; border-radius: 50%;">
                <h5 class="card-title" style="padding-left: 30%">
                    
                    <?php echo e($AuthUserInfo->name); ?>

                </h5>
                <div class="card-text" style="padding-left: 30%">
                    
                    <?php echo e($AuthUserInfo->email); ?>

                    <hr>
                    <div class="btn-group" role="group" style="text-align: center">
                        <div class="vl"></div>
                        
                        <a class="btn btn-warning btn-sm" href="../profile/<?php echo e(\Auth::user()->name); ?>" role="button">Profils</a>
                        
                        <?php if($key === 'home'): ?>

                        <?php else: ?>
                            <div class="vl"></div>
                            <a class="btn btn-danger btn-sm" href="/home" role="button">Chats</a>
                        <?php endif; ?>
                        <div class="vl"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php if(count($Friends) >0): ?>
    <div class="clearfix">
        
        <?php $__currentLoopData = $Friends; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $friend): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $__currentLoopData = $friend; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $display): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="card" style="width:100%;">
                    <div>
                        <div class="card-body" style="width: 100%">
                            <img
                                    src="<?php echo e($display->profile->avatar); ?>"
                                    style="float: left; width: 80px;height: 80px;   object-fit: cover; border-radius: 50%;"
                            >
                            <h5 class="card-title" style="padding-left: 30%">
                                <?php echo e($display->name); ?>

                            </h5>
                            <p class="card-text"
                               style="padding-left: 30%">
                                <?php echo e($display->email); ?>

                                
                                <?php if(count($display->sender)>0): ?>
                                    <?php $__currentLoopData = $display->sender; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sen): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($sen->reciever_id === \Auth::user()->id): ?>
                                            <a class="btn btn-primary" role="button" href="/chat/<?php echo e($sen->chat_key); ?>">Open
                                                Chat</a>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(count($display->reciever)>0): ?>
                                    <?php $__currentLoopData = $display->reciever; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $rec): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($rec->sender_id === \Auth::user()->id): ?>
                                            <a class="btn btn-primary" role="button" href="/chat/<?php echo e($rec->chat_key); ?>">Open
                                                Chat</a>
                                        <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </p>
                            <p class="card-text" style="clear:right">
                            </p>
                        </div>
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php endif; ?>