<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Friends;
use Illuminate\Support\Facades\Response;
use App\Models\User;
use App\Models\Chat;

class FriendController extends Controller
{
    /**
     * @param Request $req
     * @return $this
     */
    public function Request_friend(Request $req)
    {
        /*
         *get data
         */
        $reciever_id = $req->input('user_id');
        $friend = $req->input('request_friend');
        $sender_id = \Auth::user()->id;

        /*
         *checks if there aren't any same records
         */
        $validate = Friends::where('reciever_id', '=', $sender_id)->where('sender_id', '=', $reciever_id)->get();
        $checkForRepetitiveness = Friends::where('sender_id', '=', $sender_id)->where('reciever_id', '=', $reciever_id)->get();
        if (count($validate) === 1) {
            $msg = 'You already sent request';

        } elseif (count($checkForRepetitiveness) === 1) {
            $msg = 'You already sent request';
        } else {
            /*
             *Create new friend request
             */
            $store = new Friends();
            $store->sender_id = $sender_id;
            $store->reciever_id = $reciever_id;
            $store->friend = $friend;
            $store->save();
            $msg = 'Your friend request has been sent';
        }
        return redirect()->back()->withErrors([$msg]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function notify()
    {
        /*
         *get users  friend_requests and count them
         */
        $posts = Friends::where('reciever_id', '=', \Auth::user()->id)->where('friend', '=', 1)->get();
        $test = [];
        foreach ($posts as $p) {
            $test[] = User::where('id', '=', $p->sender_id)->with('profile')->get();
        }
        $count = count($test);
        $cool = ['num' => $count];
        /*
         *response array to frontend
         */
        $results = [
            'test' => $test,
            'cool' => $cool
        ];

        return Response::json($results);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function accept(Request $request)

    {
        /*
         *get data
         */
        $sender_id = $request->input('sender_id');
        $id = \Auth::user()->id;
        $approve = $request->input('approve');
        $req_id = Friends::where('sender_id', '=', $sender_id)->where('reciever_id', '=', $id)->where('friend', '=', 1)->where('aprooved', '=', 0)->first();
        //dd($req_id);
        $re = $req_id->id;
        /*
         *create new chat_key
         */
        $chatKey = utf8_encode(bin2hex(random_bytes(32)));
        /*
         *create chat
         */
        $createChat = new Chat();
        $createChat->chat_key = $chatKey;
        $createChat->first_user_id = $sender_id;
        $createChat->second_user_id = $id;
        $createChat->save();
        /*
         *update friend object so notifications disappear
         */
        $store = Friends::find($re);
        $store->sender_id = $sender_id;
        $store->reciever_id = $id;
        $store->friend = 0;
        $store->aprooved = $approve;
        $store->chat_key = $chatKey;
        $store->save();
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request)
    {
        /*
         *Delete friend request if you don't want to chat with person
         */
        $sender_id = $request->input('sender_id');
        $id = \Auth::user()->id;
        $delete = $request->input('delete');
        $req_id = Friends::where('sender_id', '=', $sender_id)->where('reciever_id', '=', $id)->where('friend', '=', 1)->where('aprooved', '=', 0)->first();
        //dd($req_id);
        $re = $req_id->id;


        $store = Friends::find($re);
        $store->delete();
        return redirect()->back();
    }

}
