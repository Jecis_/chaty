<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = ['id','first_user_id','second_user_id','changes'];

    public function messages(){
        return $this->hasMany('App\Models\Messages');
    }
    public function files(){
        return $this->hasMany('App\Models\FileHandler');
    }
}
