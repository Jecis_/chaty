<div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="searchTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="searchTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="parent">
                <input type="text" v-model.lazy="keywords" v-debounce="300" style="margin-bottom: 30px"
                       placeholder="Enter username">
                <div class="clearfix">
                    <div v-if="results.length > 0">
                        <div v-for="result in results" class="card" style="width:100%;">
                            <div>
                                <div class="card-body" style="width: 100%">


                                    <img
                                            v-bind:src="result.profile.avatar"

                                            style="float: left; width: 80px;height: 80px;   object-fit: cover; border-radius: 50%;"
                                    >

                                    <h5 class="card-title" style="padding-left: 30%"
                                        :key="result.id"
                                        v-text="result.name"
                                    >
                                    </h5>
                                    <p class="card-text" :key="result.id" v-text="result.email"
                                       style="padding-left: 30%">

                                    </p>
                                    <p class="card-text" style="clear:right">
                                    <form id="form" method="POST" action="/ask_to_be_friend"
                                          accept-charset="UTF-8">
                                        <input name="request_friend" type="hidden" value="1"/>
                                        <input name="user_id" type="hidden" v-bind:value="result.id"/>
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>

                                        <div class="form-group">
                                            <div class="control">
                                                <button class="btn btn-primary">Submit</button>
                                            </div>

                                        </div>
                                    </form>

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>