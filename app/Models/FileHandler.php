<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileHandler extends Model
{
    protected $fillable = ['chat_id', 'file_name', 'file_location', 'file_dir', 'file_type','hash_name'];

    public function chat()
    {
        return $this->hasOne('App\Models\Chat', 'id');
    }
}
