<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/css/bootstrap.min.css">
    @yield('styles')

</head>
<body>
<div id="test" style="padding-top: 200px">
    <form id="form" method="POST" action="/privateChanel"
          @submit.prevent="onSubmit"
          accept-charset="UTF-8"
          @keydown="form.errors.clear($event.target.name)"
    >
        <input id="autocomplete2" class="form-control form-control-md rounded-0"
               type="text"
               name="name"
               v-model="form.name"
        >
        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>


        <div class="form-group">
            <input class="btn btn-md u-btn-primary g-mr-10 g-mb-15" type="submit"
                   value="submit" name="submit"
            />
        </div>
    </form>
</div>
</body>
</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.2/js/bootstrap.bundle.min.js"></script>
<script src="{{asset('/assets/js/classFormAndErrors.js')}}"></script>
<script>
    const test = new Vue({
        el: '#test',
        data: {
            form: new Form({
                name: '',
            }),

        },
        methods: {
            onSubmit() {
                this.form.submit('post', '/privateChanel')

            },
        },
        created(){
            Echo.private('PrivateChanel')
                .listen('MessagesSent',(e)=>{
                    alert('yeey')
                })
        }
    });

</script>