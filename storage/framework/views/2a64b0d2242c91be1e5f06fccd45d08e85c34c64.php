<?php $__env->startSection('styles'); ?>
    <link href="<?php echo e(asset('/assets/css/chat_search.css')); ?>" rel="stylesheet" type="text/css">

<?php $__env->stopSection(); ?>


<?php echo $__env->make('partials.chat.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->startSection('content'); ?>
    
    <?php echo $__env->make('pages.chat.Modals.searchModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    

    <?php echo $__env->make('pages.chat.Modals.notificationModal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    

    <?php if($errors->any()): ?>
        <h4><?php echo e($errors->first()); ?></h4>
    <?php endif; ?>
    
    <?php echo $__env->make('pages.chat.showConvos', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>;
        window.axios.defaults.headers.common['X-CSRF-TOKEN'] = window.Laravel;
        const Auth = 'Bearer '+ '<?php echo \Auth::user()->api_token ?>';

        /*
        *Delay to wait when user stops typing in search modal
        */
        function debounce(fn, delay = 300) {
            var timeoutID = null;

            return function () {
                clearTimeout(timeoutID);

                var args = arguments;
                var that = this;

                timeoutID = setTimeout(function () {
                    fn.apply(that, args);
                }, delay);
            }
        }

        // this is where we integrate the v-debounce directive!
        // We can add it globally (like now) or locally!
        Vue.directive('debounce', (el, binding) => {
            if (binding.value !== binding.oldValue) {
                // window.debounce is our global function what we defined at the very top!
                el.oninput = debounce(ev => {
                    el.dispatchEvent(new Event('change'));
                }, parseInt(binding.value) || 300);
            }
        });
        /*
        *vue Notification instance
        */
        new Vue({
            el: '#getnotif',
            data() {
                return {
                    notification: [],
                    id: [],
                }

            },
            beforeCreate() {
                /*
                *get notifications from back-end and display
                * get notification count from back-end and
                * append it to notification btn
                */
                axios.get('/api/v1/notifications',{'headers':{'Authorization': Auth}}).then((response) => {
                    //console.log(response);
                    this.notification = response.data.test;
                    console.log(response.data);
                    $.each(response.data, function (index, value) {
                        $.each(value, function (num, value) {
                            if ($.isNumeric(value))
                                $('#num').append(value)
                        });

                    });
                });
            },
            methods: {}
        });
        /*
        *Vue search instance
        * fetch typed keys
        * send to backend
        * retrieve result
        */
        new Vue({
            el: '#parent',
            data() {

                return {
                    keywords: null,
                    results: '',

                };

            },

            watch: {
                keywords(after, before) {
                    this.fetch();
                }
            },


            methods: {

                fetch() {
                    axios.get('/api/v1/search',{
                        headers:{'Authorization': Auth},
                        params: {keywords: this.keywords}
                    })
                        .then(response => {
                            this.results = response.data.posts;
                            console.log(response.data.posts);
                        })
                        .catch(error => {
                            console.log(error)
                        });
                },

                highlight(text) {
                    return text.replace(new RegExp(this.keywords, 'gi'), '<span class="highlighted">$&</span>');
                }
            },


        });


    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.chat_layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>