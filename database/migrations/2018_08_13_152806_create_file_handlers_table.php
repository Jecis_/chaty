<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileHandlersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_handlers', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('chat_id');
            $table->text('file_name');
            $table->text('file_location');
            $table->text('file_dir');
            $table->text('file_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_handlers');
    }
}
