<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//Route::get('','ListController@search');
Route::group(['prefix' => 'v1', 'middleware' => 'auth:api'], function(){

//api routes for notifications and search must be logged in
    Route::get('notifications', 'FriendController@notify');
    Route::get('search', 'ListController@search');
    //api routes for chat send and receive messages and changes
    Route::post('/chat/msg', 'MessagesController@storeMessages');
    Route::get('/chat/listening', 'ChatController@listen')->name('listen');
    Route::get('/chat/{chat_key}/messages', 'MessagesController@getMessages');

});
Route::post('loginApi', 'API\UserController@login');
